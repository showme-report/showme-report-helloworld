import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'showme-report-hello-world',
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null
    }
  ]
};
